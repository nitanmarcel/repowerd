/*
 * Copyright © 2022 UBports foundation.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authored by: Alexander Martinz <amartinz@shiftphones.com>
 */

#pragma once

#include "src/core/double_tap_to_wake.h"

namespace repowerd
{
class Log;
class Filesystem;

struct FsDoubleTapToWakeConfig
{
    std::string path;
    std::string enable_value;
    std::string disable_value;
};

class FsDoubleTapToWake : public DoubleTapToWake
{
public:
    FsDoubleTapToWake(
        std::shared_ptr<Log> const& log,
        std::shared_ptr<Filesystem> const& filesystem);
    ~FsDoubleTapToWake();

    bool is_enabled() override;
    bool is_supported() override;

    void enable() override;
    void disable() override;

protected:
    std::shared_ptr<Log> const log;
    std::shared_ptr<Filesystem> const filesystem;
    std::vector<FsDoubleTapToWakeConfig> configs;

    void parse_config(std::string const& configvalue);

    void set_enabled(bool) override;
};

}
