/*
 * Copyright © 2022 UBports Foundation.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authored by: Marius Gripsgard <marius@ubports.com>
 */

#pragma once

#include <string>
#include <memory>

namespace repowerd
{

class DeviceInfo {
public:
    virtual ~DeviceInfo() = default;

    virtual std::string name() = 0;
    virtual bool is_desktop() = 0;

protected:
    DeviceInfo() = default;
    DeviceInfo(DeviceInfo const&) = delete;
    DeviceInfo& operator=(DeviceInfo const&) = delete;
};

}