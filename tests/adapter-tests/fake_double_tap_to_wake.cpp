/*
 * Copyright © 2022 UBports foundation.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Authored by: Alexander Martinz <amartinz@shiftphones.com>
 */

#include "fake_double_tap_to_wake.h"

namespace rt = repowerd::test;

rt::FakeDoubleTapToWake::FakeDoubleTapToWake(
        std::shared_ptr<Log> const& log,
        std::shared_ptr<Filesystem> const& filesystem)
        : repowerd::FsDoubleTapToWake(log, filesystem)
{
    auto configvalue = "/proc/touchpanel/double_tap_enable|1|0,/sys/devices/platform/soc/a84000.i2c/i2c-2/2-0020/input/input1/wake_gesture|on|off";
    parse_config(configvalue);
}
